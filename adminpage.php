
<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 1/8/16
 * Time: 1:54 PM
 */
session_start();
if ($_SESSION['email'])
    echo "Welcome, ".$_SESSION['name']." !";
else
    die("Plz login!")
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-3.3.6-dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    </head>
    <body>
    <header>
        <nav id="navbar" class="navbar navbar-inverse navbar-fixed-top" data-spy="affix" data-offset-top="200">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h3>Welcome</h3>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <nav class="nav navbar-nav navbar-right">
                        <li><a href="bookList.php">Book List</a></li>
                        <li><a href="userList.php">User List </a></li>
                        <li><a data-toggle="modal" data-target="#addBooks">Add Books</a></li>
                        <li><a href="logout.php">Logout</a></li>
                    </nav>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header>
    <div class="modal fade" tabindex="-1" role="dialog" id="addBooks">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Books</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="addBook.php" method="post">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="author">Author:</label>
                            <input type="text" class="form-control" id="author" name="author">
                        </div>
                        <div class="form-group">
                            <label for="publisher">Publisher:</label>
                            <input type="text" class="form-control" id="publisher" name="publisher">
                        </div>
                        <div class="form-group">
                            <label for="category"> Category:</label>
                            <input type="text" class="form-control" id="category" name="category">
                        </div>
                        <div class="form-group">
                            <label for="images"> Images:</label>
                            <input type="file" class="form-control" id="images" name="images">
                        </div>
                        <div class="form-group">
                            <label for="desc"> Description:</label>
                            <input type="text" class="form-control" id="desc" name="desc">
                        </div>

                        <button type="submit" id="submit" class="btn btn-default">Create</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    </body>
    <script src="assets/vendors/jquery/jquery-2.2.4.js"></script>
    <script type="text/javascript" src="assets/vendors/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
    <script type="text/javascript"></script>
</html>