<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-3.3.6-dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    </head>
    <body>
        <header>
            <nav  id="navbar" class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <h2>Welcome!!!</h2>
                </div>
            </nav>
        </header>
        <div class="modal fade" tabindex="-1" role="dialog" id="newUser">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Create New User</h4>
                    </div>
                    <div class="modal-body">
                        <form id="reg" role="form" action="registration.php" method="post">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address:</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password:</label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                            <div class="form-group">
                                <label for="confirm_password"> Confirm Password:</label>
                                <input type="password" class="form-control" id="confirm_password" onblur="confirmPass()">
                            </div>

                            <button type="submit" id="submitModal" class="btn btn-default">Create</button>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <div class="mainDivContainer">
            <div class="imageContainer">
                <img id="imgFormBackground" src="assets/images/254.jpg">
            </div>
            <div class="outerFormContainer">
                <h3 class="textColorWhite">Login</h3>
                <div class="innerFormContainer">
                    <form role="form" action="login.php" method="post">
                        <div class="form-group">
                            <label for="email" class="textColorWhite">Email address:</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="pwd" class="textColorWhite">Password:</label>
                            <input type="password" class="form-control" id="pwd" name="pwd">
                        </div>
                        <div class="center">
                            <button type="submit" id="submit" class="btn btn-default">Login</button>
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#newUser">Create New User</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script src="assets/vendors/jquery/jquery-2.2.4.js"></script>
    <script type="text/javascript" src="assets/vendors/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
    <script type="text/javascript">
        function confirmPass() {
            var password = document.getElementById("password").value
            var confirm_password = document.getElementById("confirm_password").value
            if(password != confirm_password) {
                alert('Wrong confirm password !');
                $("#reg").submit(function(e){
                    e.preventDefault();
                })
            }else
            {
                $("#reg").unbind('submit');
            }
        }
    </script>
</html>