<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 13/8/16
 * Time: 9:05 PM
 */

session_start();
include 'config.php';
?>
    <!DOCTYPE html>
    <html>
<?php
if($_SESSION['roll']=='admin') {
    include 'headerAdmin.php';
}else{
    include 'headerMember.php';
}
?>
<body>
<div class="container">
    <h2 class="text-center">Currently Used Books</h2>
    <table class="table">
        <thead>
        <tr>
            <th>Book Name</th>
            <th>User Name</th>
            <th>Status</th>
            <th>Issue Date</th>
        </tr>
        </thead>
        <?php
        $sql="SELECT `book`.`name` AS 'bookName', `user`.`name` AS 'userName',`transaction`.`status`, `transaction`.`issueDate` FROM `transaction` JOIN `book` ON (`transaction`.`bookId`=`book`.`id` AND `transaction`.`status`='issued') join `user` on (`transaction`.`uId`=`user`.`uId`)";
        $result= $conn->query($sql);
        while($row= $result->fetch_assoc()) {
            ?>
            <tbody>
            <tr>
                <td><?php echo $row['bookName'] ?></td>
                <td><?php echo $row['userName'] ?></td>
                <td><?php echo $row['status'] ?></td>
                <td><?php echo $row['issueDate'] ?></td>
            </tr>
            </tbody>
            <?php
        }
        ?>
    </table>
</body>
</html>

<?php
    $conn->close();
?>
