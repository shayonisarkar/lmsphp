<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-3.3.6-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <script src="assets/vendors/jquery/jquery-2.2.4.js"></script>
    <script type="text/javascript" src="assets/vendors/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
</head>
<body>
<?php

session_start();
include 'config.php';

if($_SESSION['roll']=='admin') {
    include 'headerAdmin.php';
}else{
    include 'headerMember.php';
}

$limit= 4;
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
$start_from = ($page-1) * $limit;

$sql = "SELECT * FROM user ORDER BY uId ASC LIMIT $start_from, $limit";
$result = $conn->query($sql);
echo '<table class="table tableUserList">';
echo '<thead class="thead-userList">';
echo '<tr>';
    echo '<th>Name</th>';
    echo '<th>Email</th>';
    echo '<th>Password</th>';
    echo '<th>Edit</th>';
    echo '<th>Delete</th>';
echo '</tr>';
echo '</thead>';
echo '<tbody class="tbody-userList">';
while ($row = $result->fetch_assoc()) {
    //    echo '<li>'.$row['id'].'<li>';
    echo '<tr>';
    echo '<td>'.$row['name'].'</td>';
    echo '<td>'.$row['email'].'</td>';
    echo '<td>'.$row['password'].'</td>';

    echo '<td><a href="updateUser.php?index1='.$row['email'].'">Edit</a></td>';
    echo '<td><a href="deleteUser.php?index2='.$row['email'].'"> Delete</a></td>';
    echo '</tr>';
}
echo '<tbody>';
echo '</table>';
?>
<div class="modal fade" tabindex="-1" role="dialog" id="addBooks">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Books</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="addBook.php" method="post">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="author">Author:</label>
                        <input type="text" class="form-control" id="author" name="author">
                    </div>
                    <div class="form-group">
                        <label for="publisher">Publisher:</label>
                        <input type="text" class="form-control" id="publisher" name="publisher">
                    </div>
                    <div class="form-group">
                        <label for="category"> Category:</label>
                        <input type="text" class="form-control" id="category" name="category">
                    </div>
                    <div class="form-group">
                        <label for="images"> Images:</label>
                        <input type="file" class="form-control" id="images" name="images">
                    </div>
                    <div class="form-group">
                        <label for="desc"> Description:</label>
                        <input type="text" class="form-control" id="desc" name="desc">
                    </div>

                    <button type="submit" id="submit" class="btn btn-default">Create</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
$tot="SELECT COUNT(`uId`) FROM `user`";
$total_result= $conn->query($tot);
/*echo $total_result;*/
/*var_dump($total_result->fetch_assoc());*/
$total_records=$total_result->fetch_assoc();
$data=$total_records ['COUNT(`uId`)'];
$total_pages = ceil($data / $limit);
?>

<nav aria-label="Page navigation">
    <ul class="pagination displayPagination">
        <li class="page-item">
            <a class="page-link paginationUserButton" href="?page=1" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        <?php
        for ($i=1; $i<=$total_pages; $i++) {

              echo "<li><a class='page-link paginationUserButton' href='$_PHP_SELF?page=".$i."'>".$i."</a></li>";
        }
        if (($i-1)==$total_pages){
        ?>

        <li>
            <a class="page-link paginationUserButton" href="?page=<?php echo $total_pages ?>" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
        <?php
            }
        ?>
    </ul>
</nav>
</body>

</html>
<?php
$conn->close();
?>