<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 4/8/16
 * Time: 1:02 PM
 */

session_start();

include 'config.php';

$uid=$_SESSION['uId'];
$id = $_GET['indexStkAdd'];
$bookName=$_GET['book_name']
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-3.3.6-dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <script src="assets/vendors/jquery/jquery-2.2.4.js"></script>
        <script type="text/javascript" src="assets/vendors/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
        <script type="text/javascript" src="assets/vendors/bootstrap-3.3.6-dist/bootstrap-datepicker-1.6.1-dist/js/bootstrap-datepicker.js"></script>

    </head>
    <?php
    if($_SESSION['roll']=='admin') {
        include 'headerAdmin.php';
    }else{
        include 'headerMember.php';
    }
    ?>
    <body>
        <form role="form" method="post" action="" class="userUpdate">
            <h3 class="text-center">Add Stock</h3>
            <div class="form-group" hidden>
                <label for="userid">User ID</label>
                <input type="text" class="form-control" id="userid" name="userid" value="<?php echo $uid ?>">
            </div>
            <div class="form-group" hidden>
                <label for="bookId">Book Id</label>
                <input type="text" class="form-control" name="bookId" id="bookId" readonly value="<?php echo $id ?>">
            </div>
            <div class="form-group">
                <label for="bookName">Book Name</label>
                <input type="text" class="form-control" name="bookName" id="bookName" readonly value="<?php echo $bookName ?>">
            </div>
            <div class="form-group">
                <label for="date">Date</label>
                <input type="date" class="form-control" name="date" id="date" readonly value="<?php echo date("Y-m-d") ?>">
            </div>
            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="text" class="form-control" name="quantity" id="quantity">
            </div>
            <div class="form-group">
                <label for="tType">Transaction Type</label>
                <select class="form-control" name="tType" id="tType">
                    <option value="Add Stock">Add Stock</option>
                    <option value="Remove Stock"> Remove Stock</option>
                </select>
            </div>
            <div class="text-center"><button type="submit" name="bts" value="send" class="btn btn-default">Submit</button></div>
        </form>
    </body>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        $.fn.datepicker.defaults.format = "yyyy/mm/dd/";
        $( "#datepicker" ).datepicker();
    </script>
</html>

<?php
$userid = $_POST['userid'];
$bookId = $_POST['bookId'];
$date = $_POST['date'];
$quantity=$_POST['quantity'];
$tType = $_POST['tType'];
$status= "NONE";
/*if(isset($_POST['bts'])){*/
if(!empty($userid&&$bookId&&$date&&$quantity&&$tType)){
    if($tType=="Add Stock") {
        $sql = "INSERT INTO `transaction` (`bookId`,`uId`,`tDate`,`quantity`,`tType`,`status`) VALUES ('$bookId','$userid','$date','$quantity','$tType','$status')";
        if ($conn->query($sql) === TRUE) {
            ?>
            <div class="alert alert-info text-center">
                <strong>New record created successfully!</strong>
            </div>
            <?php
            header("Refresh:3");
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    else{
        $sql = "INSERT INTO `transaction` (`bookId`,`uId`,`tDate`,`quantity`,`tType`,`status`) VALUES ('$bookId','$userid','$date','-$quantity','$tType','$status')";
        if ($conn->query($sql) === TRUE) {
            ?>
            <div class="alert alert-info text-center">
                <strong>Removed successfully!</strong>
            </div>
            <?php
            header("Refresh:3");
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    $conn->close();
}

?>
