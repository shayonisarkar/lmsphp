<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 13/8/16
 * Time: 12:48 PM
 */
include 'config.php';
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-3.3.6-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <script src="assets/vendors/jquery/jquery-2.2.4.js"></script>
    <script type="text/javascript" src="assets/vendors/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
    <script src="assets/vendors/jquery-ui-1.12.0.custom/jquery-ui.js"></script><!--for autocomplete-->
    <link rel="stylesheet" href="assets/vendors/jquery-ui-1.12.0.custom/jquery-ui.css">
</head>
<body>

<header>
    <nav id="navbar" class="navbar navbar-inverse navbar-fixed-top" data-spy="affix" data-offset-top="200">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h3>Welcome</h3>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <nav class="nav navbar-nav navbar-right">
                    <li><a href="bookList.php">Book List</a></li>
                    <?php
                    if($_SESSION['roll']=="admin"){
                        ?>
                        <li><a href="userList.php">User List</a></li>
                        <li><a data-toggle="modal" data-target="#addBooks">Add Books</a></li>
                        <div class="dropdown headerDropdownLink">
                            <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown">Reports
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="bookStock.php">Book Stock</a></li>
                                <li><a href="currentlyIssuedBook.php">Book Currently Issued</a></li>
                            </ul>
                        </div>
                        <?php
                    }else{
                        ?>
                        <div class="dropdown headerDropdownLink">
                        <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown">Reports
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="issueListMember.php">Your Issued Book History</a></li>
                        </ul>
                    </div>
                    <?php
                    }
                    ?>
                    <li><a href="logout.php">Logout</a></li>
                </nav>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>
<?php

/*-----------------------------------------* pagination *-----------------------------------------*/
    $limit= 2;
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
    $start_from = ($page-1) * $limit;

/*-----------------------------------------* counting the number of book issued *-----------------------------------------*/

    $countBookIssued="SELECT COUNT(`tId`) FROM transaction where `uId`='".$userId['uId']."' && `status`='issued'";
    $resultCount = $conn->query($countBookIssued);
    $total_count=$resultCount->fetch_assoc();
    $countIssued=$total_count ['COUNT(`tId`)'];

/*-----------------------------------------* filter *-----------------------------------------*/

    echo "<form method='post' action='' class='filter container'>";
/*        echo "<div class='form-group'>";
            $sortByName="SELECT * FROM `book`  order by `name`";
            echo "<select id='sortByBookName' name='sortByBookName' value='sortByBookName' class='form-control'>";
            echo "<option value=''>Select Book Name</option>";
            foreach ($conn->query($sortByName) as $row)
            {            //Array or records stored in $row
                echo "<option value='".$row[name]."'>$row[name]</option>";
            }
            echo "</select>";
        echo "</div>";*/
        echo "<div class='form-group ui-widget'>";/*filter by book name*/
            echo "<label for='bookName'>Name of the Book : </label>";
            echo "<input type='text' class='form-control' name='bookName' id='bookName' placeholder='Type the Book Name'>";
        echo "</div>";

        echo "<div class='form-group ui-widget'>";/*filter by author*/
            echo "<label for='authorName'>Name of the Author : </label>";
            echo "<input type='text' class='form-control' name='authorName' id='authorName' placeholder='Type the Author Name'>";
        echo "</div>";
/*        echo "<div class='form-group'>";
            $sortByAuthor="SELECT DISTINCT `author`, `id` FROM `book` ORDER BY `author`";
            echo "<select id='sortByBookAuthor' name='sortByBookAuthor' value='sortByBookAuthor' class='form-control'>";
            echo "<option value=''>Select Author Name</option>";
            foreach ($conn->query($sortByAuthor) as $row)
            {            //Array or records stored in $row
                echo "<option value='".$row[author]."'>$row[author]</option>";
            }
            echo "</select>";
        echo "</div>";*/

    echo "<div class='text-center'><button name='submit' class='btn btn-default' type='submit' value='submit'>Select Book</button>";
    echo "<button type='button' class='btn btn-default' value='Refresh Page' onClick='refresh()'>Refresh</button></div>";

    echo "</form>";
    if ( isset($_POST['submit']) ) {
        $bookName= $_POST['bookName'];
        $authorName= $_POST['authorName'];
    }



/*-----------------------------------------* table *-----------------------------------------*/
    echo '<div class="container">';
    echo '<table class="table tableBookList">';
/*-----------* query acc. to filter *----------*/
    if($bookName==''&&$authorName==''){
        $sql = "SELECT * FROM book ORDER BY id ASC LIMIT $start_from, $limit";
        $countFiltered = "SELECT COUNT(`id`) FROM book ORDER BY id ASC";
    }elseif($bookName!=''&&$authorName==''){
        $sql = "SELECT * FROM book WHERE `name`='".$bookName."' ORDER BY id ASC LIMIT $start_from, $limit";
        $countFiltered = "SELECT COUNT(`id`) FROM book WHERE `name`='".$bookName."' ORDER BY id ASC";
    }elseif($bookName==''&&$authorName!=''){
        $sql = "SELECT * FROM book WHERE `author`='".$authorName."' ORDER BY id ASC LIMIT $start_from, $limit";
        $countFiltered = "SELECT COUNT(`id`) FROM book WHERE `author`='".$authorName."' ORDER BY id ASC";
    }elseif($bookName!=''&&$authorName!=''){
        $sql = "SELECT * FROM book WHERE `name`='".$bookName."' && `author`='".$authorName."' ORDER BY id ASC LIMIT $start_from, $limit";
        $countFiltered = "SELECT COUNT(`id`) FROM book WHERE `name`='".$bookName."' && `author`='".$authorName."' ORDER BY id ASC";
        $constant=1;
    }
    $result = $conn->query($sql);
    $resultCountFiltered = $conn->query($countFiltered);
    $filteredRecords=$resultCountFiltered->fetch_assoc();
    $filteredData= $filteredRecords['COUNT(`id`)'];
    if($filteredData==0 && $constant=1){

        echo "<div class='alert alert-danger container alertNoBookFound'>";
            echo "<strong>No Book Found</strong>";
        echo "</div>";
    }else {
        echo '<thead class="thead-bookList">';
        echo '<tr>';
        echo '<th>#</th>';
        echo '<th>Name</th>';
        echo '<th>Author</th>';
        echo '<th>Publisher</th>';
        echo '<th>Category</th>';
        echo '<th>Images</th>';
        echo '<th>Description</th>';
        if ($_SESSION['roll'] == "admin") {
            echo '<th>Edit</th>';
            echo '<th>Delete</th>';
            echo '<th>Manage Stock</th>';
            echo '<th>Return Book</th>';

        }
        echo '<th>Issue Book</th>';

        echo '</tr>';
        echo '</thead>';
        echo '<tbody class="tbody-bookList">';

        while ($row = $result->fetch_assoc()) {
            echo '<tr>';
            echo '<td>' . $row['id'] . '</td>';
            echo '<td>' . $row['name'] . '</td>';
            echo '<td>' . $row['author'] . '</td>';
            echo '<td>' . $row['publisher'] . '</td>';
            echo '<td>' . $row['category'] . '</td>';
            echo '<td><img src="assets/images/' . $row['images'] . '" class="imagetd"></td>';
            echo '<td>' . $row['desc'] . '</td>';
            if ($_SESSION['roll'] == "admin") {
                echo '<td><a href="update.php?u=' . $row['id'] . '">Edit</a></td>';
                echo '<td><a href="delete.php?indexDel=' . $row['id'] . '"> Delete</a></td>';
                echo '<td><a href="addStock.php?indexStkAdd=' . $row['id'] . '&book_name=' . $row['name'] . '">Manage</a></td>';
                echo '<td><a href="return.php?return=' . $row['id'] . '&book_name=' . $row['name'] . '">Return</a></td>';
            }
            $trans = "SELECT * FROM `transaction` WHERE (bookId='" . $row['id'] . "' && uId='" . $_SESSION['uId'] . "') ORDER BY `tId` DESC LIMIT 1;";
            $result_trans = $conn->query($trans);
            $row_trans = $result_trans->fetch_assoc();
            if ($_SESSION['roll'] != "admin") {
                if ($countIssued < 1) {
                    if (($row_trans['status']) == "issued") {
                        echo '<td><a href="issuebook.php?indexIssue=' . $row['id'] . '&book_name=' . $row['name'] . '" >Issued</a></td>';
                    } else {
                        echo '<td><a id="issueBookCount" data-id="' . $row['id'] . '" data-name="' . $row['name'] . '" href="#" onclick="checkBookLeft(this)" >Issue </a></td>';
                    }
                    echo '</tr>';
                } else {
                    if (($row_trans['status']) == "issued") {
                        echo '<td><a class="" data-toggle="modal" data-target="#myModal" href="#myModal">Issued</a></td>';
                    } else {
                        echo '<td><a data-toggle="modal" data-target="#myModal" href="#myModal">Issue</a></td>';
                        echo '</tr>';
                    }
                }
            } else {
                if ($countIssued < 1) {
                    if (($row_trans['status']) == "issued") {
                        echo '<td><a href="issuebook.php?indexIssue=' . $row['id'] . '&book_name=' . $row['name'] . '" >Issued</a></td>';
                    } else {
                        echo '<td><a href="issuebook.php?id=' . $row['id'] . '&book_name=' . $row['name'] . '" >Issue </a></td>';
                    }
                    echo '</tr>';
                } else {
                    if (($row_trans['status']) == "issued") {
                        echo '<td><a class="" data-toggle="modal" data-target="#myModal" href="#myModal">Issued</a></td>';
                    } else {
                        echo '<td><a data-toggle="modal" data-target="#myModal" href="#myModal">Issue</a></td>';
                        echo '</tr>';
                    }
                }

            }
        }
        echo '</tbody>';
        echo '</table>';
        echo '</div>';
    }
    ?>


<!------------------------------------------modal if book already issued----------------------------------------->

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-group">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <label><h3>You have already issued a book</h3></label>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

<!------------------------------------------modal for adding books ----------------------------------------->

    <div class="modal fade" tabindex="-1" role="dialog" id="addBooks">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Books</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="addBook.php" method="post">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="author">Author:</label>
                            <input type="text" class="form-control" id="author" name="author">
                        </div>
                        <div class="form-group">
                            <label for="publisher">Publisher:</label>
                            <input type="text" class="form-control" id="publisher" name="publisher">
                        </div>
                        <div class="form-group">
                            <label for="category"> Category:</label>
                            <input type="text" class="form-control" id="category" name="category">
                        </div>
                        <div class="form-group">
                            <label for="images"> Images:</label>
                            <input type="file" class="form-control" id="images" name="images">
                        </div>
                        <div class="form-group">
                            <label for="desc"> Description:</label>
                            <input type="text" class="form-control" id="desc" name="desc">
                        </div>

                        <button type="submit" id="submit" class="btn btn-default">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
    if($bookName==''&& $authorName=='') {
        $tot = "SELECT COUNT(`id`) FROM `book`";
    }elseif($bookName!=''&&$authorName==''){
        $tot = "SELECT COUNT(`id`) FROM `book` WHERE `name`='".$bookName."'";
    }elseif($bookName==''&&$authorName!=''){
        $tot = "SELECT COUNT(`id`) FROM `book` WHERE `author`='".$authorName."'";
    }elseif($bookName!=''&&$authorName!=''){
        $tot = "SELECT COUNT(`id`) FROM `book` WHERE `name`='".$bookName."' && `author`='".$authorName."'";
    }
    $total_result= $conn->query($tot);
    $total_records=$total_result->fetch_assoc();
    $data=$total_records ['COUNT(`id`)'];
    $total_pages = ceil($data / $limit);
    if($total_pages!=0){
    ?>
    <nav aria-label="Page navigation">
        <ul class="pagination displayPagination">
            <li class="page-item">
                <a class="page-link paginationBookListButton" href="?page=1" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            <?php
            for ($i=1; $i<=$total_pages; $i++) {

                echo "<li><a class='page-link paginationBookListButton' href='$_PHP_SELF?page=".$i."'>".$i."</a></li>";
            }
            if (($i-1)==$total_pages){
            ?>
            <li>
                <a class="page-link paginationBookListButton" href="?page=<?php echo $total_pages ?>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
                <?php
            }
            ?>
        </ul>
    </nav>
    <?php
    }
    ?>

    <div id="alertBookLeft" class="alert alert-info hidden">
        <span id="bookCount"></span>
    </div>
</body>
<script type="text/javascript">
    function checkBookLeft(obj){
        var id = obj.getAttribute("data-id");
        var name = obj.getAttribute("data-name");

/*        console.log(dataId);
         console.log(dataName);*/


        if (issueBookCount) {
            /*alert(name);*/
                $.ajax({
                type: 'get',
                url: 'issuebook.php',
                dataType: 'json',
                data: {
                    name: name,
                    id:id
                },
                success: function (response) {
                    console.log(response);
                  $('#bookCount').text(response);
                    $("#alertBookLeft").removeClass("hidden");
                    setTimeout(function() { window.location.reload(true); }, 5000);
                }
            });
        }
        else {
            $('#bookCount').html("Please Enter Some Words");
        }
    }

    function refresh() {
        window.location = 'http://localhost/lmsphp/bookList.php';
    }

    $(document).ready(function(){
        $( "#bookName" ).autocomplete({
            source: function(request, response) {
                $.getJSON("searchBook.php", {
                    bookName: $('#bookName').val()
                }, response);
            }
        });
        $( "#authorName" ).autocomplete({
            source: function(request, response) {
                $.getJSON("searchAuthor.php", {
                    authorName: $('#authorName').val()
                }, response);
            }
        });
    });
</script>

</html>