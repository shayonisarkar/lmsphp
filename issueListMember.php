<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 13/8/16
 * Time: 5:34 PM
 */

session_start();
include 'config.php';
?>
    <!DOCTYPE html>
    <html>
<?php
if($_SESSION['roll']=='admin') {
    include 'headerAdmin.php';
}else{
    include 'headerMember.php';
}
?>
<body>
<h2 class="text-center">Users Issued Book History</h2>
<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th>Book Name</th>
            <th>Status</th>
            <th>Issue Date</th>
            <th>Return Date</th>
        </tr>
        </thead>
        <?php
        $sql="SELECT `book`.`name`, `transaction`.`status`, `transaction`.`issueDate`, `transaction`.`returnDate` FROM `transaction` 	JOIN `book` ON (`transaction`.`bookId`=`book`.`id` AND `transaction`.`uId`='".$_SESSION['uId']."' AND `transaction`.`tType`='Issue Book')";
        $result= $conn->query($sql);
        while($row= $result->fetch_assoc()) {
            ?>

            <tbody>
            <tr>
                <td><?php echo $row['name'] ?></td>
                <td><?php echo $row['status'] ?></td>
                <td><?php echo $row['issueDate'] ?></td>
                <td><?php echo $row['returnDate'] ?></td>
            </tr>
            </tbody>
            <?php
        }
        ?>
    </table>
</div>
</body>
</html>

<?php
$conn->close();
?>
