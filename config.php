<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 13/8/16
 * Time: 12:55 PM
 */
$servername = "localhost";
$username = "root";
$password = "password";
$dbname = "phplms";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>