<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 30/7/16
 * Time: 9:05 PM
 */
session_start();
if ($_SESSION['email'])
    echo "Welcome, ".$_SESSION['name']." !"."<a href='logout.php'>logout</a>";
else
    die("Plz login!");
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-3.3.6-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
<header>
    <nav id="navbar" class="navbar navbar-inverse navbar-fixed-top" data-spy="affix" data-offset-top="200">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                </button>
                <h3>Welcome</h3>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <nav class="nav navbar-nav navbar-right">
                    <li><a href="bookList.php">Book List</a></li>
                    <li><a href="logout.php">Logout</a></li>
                </nav>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>

</body>
<script src="assets/vendors/jquery/jquery-2.2.4.js"></script>
<script type="text/javascript" src="assets/vendors/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
</html
