<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 5/8/16
 * Time: 12:13 PM
 */

session_start();

include 'config.php';
?>
<?php
$name = $_SESSION['name'];
$id= $_GET['id'];



$countBookLeft = "SELECT SUM(  `quantity` ) FROM `transaction` WHERE `bookId`='" . $id . "'";
$resultBookLeft= $conn->query($countBookLeft);
$rowBookLeft= $resultBookLeft->fetch_assoc();
$dataBookLeft = $rowBookLeft['SUM(  `quantity` )'];
if($dataBookLeft>2) {

    if ($_SESSION['roll'] == 'admin') {
        ?>


        <!DOCTYPE html>
        <html>
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-3.3.6-dist/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="assets/css/style.css">
            <script src="assets/vendors/jquery/jquery-2.2.4.js"></script>
            <script type="text/javascript" src="assets/vendors/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
        </head>
        <?php
        if($_SESSION['roll']=='admin') {
            include 'headerAdmin.php';
        }else{
            include 'headerMember.php';
        }
        ?>
        <body>
        <form role="form" method="post" action="" class="userUpdate">
            <h3 class="text-center">Issue Book</h3>
            <div class="form-group">
                <label for="name">User Name</label>
                <input type="text" class="form-control" id="name2" name="name" onblur="loaddata()"
                       value="<?php echo $name?>">
                <!--<div><span id="display_info" > </span></div>-->
            </div>
            <div class="form-group">
                <label for="display_info">User Id</label>
                <input type="text" class="form-control" id="display_info1" name="display_info">
            </div>
            <div class="text-center">
                <button type="submit" name="bts" value="send" class="btn btn-default">Submit</button>
            </div>
        </form>
        </body>
        <script type="text/javascript">
            function loaddata() {
                /* console.log('Logged');*/
                var name = document.getElementById("name2").value;
                if (name) {
                    /*alert(name);*/
                    $.ajax({
                        type: 'post',
                        url: 'loaddata.php',
                        dataType: 'json',
                        data: {
                            user_name: name
                        },
                        success: function (response) {
                            // We get the element having id of display_info and put the response inside it
                            $('#display_info1').val(response);
                        }
                    });
                }
                else {
                    $('#display_info').html("Please Enter Some Words");
                }
            }

        </script>
        </html>
        <?php
        if (isset($_POST['bts']) == 'send') {
            $uid = $_POST['display_info'];
            $id = $_GET['id'];
            $date = date("Y-m-d");
            $quantity = "-1";
            $tType = "Issue Book";
            $status = "issued";
            $issueDate = date("Y-m-d");
            $returnDate = "0000-00-00";


            $sql = "INSERT INTO `transaction` (`bookId`,`uId`,`tDate`,`quantity`,`tType`,`status`,`issueDate`,`returnDate`) VALUES ('$id','$uid','$date','$quantity','$tType','$status','$issueDate','$returnDate')";
            if ($conn->query($sql) === TRUE) {
                header("Refresh:0; url=bookList.php");;
                echo "successful";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }
        ?>
        <?php
    } else {
        ?>

        <?php

        $uid = $_SESSION['uId'];
        $id = $_GET['id'];
        $date = date("Y-m-d");
        $quantity = "-1";
        $tType = "Issue Book";
        $status = "issued";
        $issueDate = date("Y-m-d");
        $returnDate = "0000-00-00";

        $sql = "INSERT INTO `transaction` (`bookId`,`uId`,`tDate`,`quantity`,`tType`,`status`,`issueDate`,`returnDate`) VALUES ('$id','$uid','$date','$quantity','$tType','$status','$issueDate','$returnDate')";
        if ($conn->query($sql) === TRUE) {
            ?>
            <script type="text/javascript">
                /*window.location.reload();*/
                window.location = 'http://localhost/lmsphp/bookList.php';
            </script>
            <?php
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        ?>
        <?php
    }
}else{
    echo json_encode("Sorry... No Book Left!!!");
    header("Refresh:3; url= bookList.php");
}

$conn->close();
?>
