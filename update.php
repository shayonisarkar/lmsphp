<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 2/8/16
 * Time: 4:49 PM
 */
session_start();
include 'config.php';

$id = $_GET['u'];
/*echo $id;*/

$sql = "SELECT * FROM book WHERE id=".$_GET['u'];
$result = $conn->query($sql);
$row = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-3.3.6-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/images">
    <script src="assets/vendors/jquery/jquery-2.2.4.js"></script>
    <script type="text/javascript" src="assets/vendors/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
</head>
<body>
<?php
if($_SESSION['roll']=='admin') {
    include 'headerAdmin.php';
}else{
    include 'headerMember.php';
}
?>
<form role="form" method="post" action="" class="formUpdate">
        <h3 class="text-center">Update Book List</h3>
        <input type="hidden" value="<?php echo $row['id'] ?>" name="id"/>
        <div class="form-group">
          <label for="nm">Name</label>
          <input type="text" class="form-control" name="name" id="nm" value="<?php echo $row['name'] ?>">
        </div>
        <div class="form-group">
          <label for="author">Author</label>
          <input type="text" class="form-control" id="author" name="author"value="<?php echo $row['author'] ?>">
        </div>
        <div class="form-group">
            <label for="publisher">Publisher</label>
            <input type="text" class="form-control" name="publisher" id="publisher" value="<?php echo $row['publisher'] ?>">
        </div>
        <div class="form-group">
            <label for="category">Category</label>
            <input type="text" class="form-control" name="category" id="category" value="<?php echo $row['category'] ?>">
        </div>
        <div class="form-group">
            <label for="images">Images</label>

            <div><img src=assets/images/<?=$row['images'] ?> alt="test" class="imageUpdate"/></div>
            <input type="file" class="form-control"  name="images" id="images" value="<?php echo $row['images'] ?>">
        </div>
        <div class="form-group">
            <label for="images">Description</label>
            <input type="text" class="form-control" name="desc" id="desc" value="<?php echo $row['desc'] ?>">
        </div>
        <div class="text-center">
        <button type="submit" name="bts" class="btn btn-default submit">Submit</button></div>
    </form>
    <div class="modal fade" tabindex="-1" role="dialog" id="addBooks">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Books</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="addBook.php" method="post">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="author">Author:</label>
                            <input type="text" class="form-control" id="author" name="author">
                        </div>
                        <div class="form-group">
                            <label for="publisher">Publisher:</label>
                            <input type="text" class="form-control" id="publisher" name="publisher">
                        </div>
                        <div class="form-group">
                            <label for="category">Category:</label>
                            <input type="text" class="form-control" id="category" name="category">
                        </div>
                        <div class="form-group">
                            <label for="images"> Images:</label>
                            <input type="file" class="form-control" id="images" name="images">
                        </div>
                        <div class="form-group">
                            <label for="desc"> Description:</label>
                            <input type="text" class="form-control" id="desc" name="desc">
                        </div>

                        <button type="submit" id="submit" class="btn btn-default">Create</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</body>

<?php
if(!empty($_POST)) {
    $name = $_POST['name'];
    $author = $_POST['author'];
    $publisher = $_POST['publisher'];
    $category = $_POST['category'];
    $images = $_POST['images'];
    $description = $_POST['desc'];

    $update = "UPDATE book SET `name`='".$name."',`author`='".$author."',`publisher`='".$publisher."',`category`='".$category."',`images`='".$images."',`desc`='".$description."' WHERE id=$id";
    /*$res = $conn->query($update);*/
    if ($conn->query($update) === TRUE) {
        header("Refresh:6");
        echo "New record created successfully";
    } else {
        echo "Error: " . $update . "<br>" . $conn->error;
    }
}
?>
</html>
<?php
$conn->close();
?>
