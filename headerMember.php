<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 13/8/16
 * Time: 1:07 PM
<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 13/8/16
 * Time: 11:53 AM
 */
?>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-3.3.6-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <script src="assets/vendors/jquery/jquery-2.2.4.js"></script>
    <script type="text/javascript" src="assets/vendors/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
</head>
<header>
    <nav id="navbar" class="navbar navbar-inverse navbar-fixed-top" data-spy="affix" data-offset-top="200">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h3>Welcome</h3>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <nav class="nav navbar-nav navbar-right">
                    <li><a href="bookList.php">Book List</a></li>
                    <div class="dropdown headerDropdownLink">
                        <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown">Reports
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="issueListMember.php">Your Issued Book History</a></li>
                        </ul>
                    </div>
                    <li><a href="logout.php">Logout</a></li>
                </nav>
            </div>
        </div>
    </nav>
</header>
