<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 13/8/16
 * Time: 12:48 PM
 */

    session_start();
    include 'config.php';
?>
<!DOCTYPE html>
<html>
<?php
    if($_SESSION['roll']=='admin') {
        include 'headerAdmin.php';
    }else{
        include 'headerMember.php';
    }
?>
<body>
    <div class="container">
    <table class="table">
        <thead>
        <tr>
            <th>Book Name</th>
            <th>Book Id</th>
            <th>Stock Quantity</th>
            <th>Images</th>
        </tr>
        </thead>
<?php
/*-----------------------------------------* pagination *-----------------------------------------*/
$limit= 3;
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
$start_from = ($page-1) * $limit;
/*------------------------------------------------------------------------------------------------*/

$sql="SELECT DISTINCT(`book`.`name`), `book`.`id`, `book`.`images`, SUM(`transaction`.`quantity`) FROM `transaction` RIGHT JOIN `book` ON (`transaction`.`bookId`=`book`.`id` AND  (`transaction`.`tType`='Add Stock' OR `transaction`.`tType`='Remove Stock')) GROUP BY `book`.`name`  LIMIT $start_from, $limit";
$result= $conn->query($sql);
while($row= $result->fetch_assoc()) {
    $quantity = $row['SUM(`transaction`.`quantity`)'];
    ?>

        <tbody>
        <tr>
            <td><?php echo $row['name'] ?></td>
            <td><?php echo $row['id'] ?></td>
            <td><?php
                if ($quantity==''){
                    echo '0';
                }
                else{
                    echo $quantity;
                }?></td>
            <td><img class="imagetd" src='../lmsphp/assets/images/<?php echo $row['images'] ?>'</td>
        </tr>
        </tbody>
        <?php
}

$numberCount="SELECT COUNT(DISTINCT(`book`.`name`)) FROM `transaction` RIGHT JOIN `book` ON (`transaction`.`bookId`=`book`.`id` AND  (`transaction`.`tType`='Add Stock' OR `transaction`.`tType`='Remove Stock'))";
$resultNumberCount= $conn->query($numberCount);
$rowCount=$resultNumberCount->fetch_assoc();
$dataCount= $rowCount['COUNT(DISTINCT(`book`.`name`))'];

$limit=3;
$total_pages = ceil($dataCount / $limit);
?>
    </table>
    </div>
<?php
if($total_pages!=0){
    ?>
    <nav aria-label="Page navigation">
        <ul class="pagination displayPagination">
            <li class="page-item">
                <a class="page-link " href="?page=1" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            <?php
            for ($i=1; $i<=$total_pages; $i++) {

                echo "<li><a class='page-link ' href='$_PHP_SELF?page=".$i."'>".$i."</a></li>";
            }
            if (($i-1)==$total_pages){
                ?>
                <li>
                    <a class="page-link " href="?page=<?php echo $total_pages ?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
                <?php
            }
            ?>
        </ul>
    </nav>
    <?php
}
?>
?>
</body>
</html>
<?php
$conn->close();
?>
