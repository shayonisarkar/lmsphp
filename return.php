<?php
/**
 * Created by PhpStorm.
 * User: shayoni
 * Date: 8/8/16
 * Time: 11:32 AM
 */

session_start();
include 'config.php';
$name = $_SESSION['name'];
if($_SESSION['roll']=='admin') {
    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-3.3.6-dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    </head>
    <body>
    <?php
    if($_SESSION['roll']=='admin') {
        include 'headerAdmin.php';
    }else{
        include 'headerMember.php';
    }
    ?>
    <form role="form" method="post" action="" class="">
        <h3 class="text-center">Return Book</h3>
        <div class="form-group">
            <label for="name">User Name</label>
            <input type="text" class="form-control" id="name1" name="name" onblur="loaddata();   "
                   value="<?php echo $name ?>">
            <!--<div><span id="display_info" > </span></div>-->
        </div>
        <div class="form-group">
            <label for="display_info">User Id</label>
            <input type="text" class="form-control" id="display_info1" name="display_info">
        </div>
        <div class="text-center">
            <button type="submit" name="bts" value="send" class="btn btn-default">Submit</button>
        </div>
    </form>
    </body>
    <script src="assets/vendors/jquery/jquery-2.2.4.js"></script>
    <script type="text/javascript" src="assets/vendors/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
    <script type="text/javascript">
        function loaddata() {
             /*console.log('Logged');*/
            var name = document.getElementById("name1").value;
            /*alert(name);*/
            if (name) {
                /*alert(name);*/
               $.ajax({
                    type: 'post',
                    url: 'loaddata.php',
                    dataType: 'json',
                    data: {
                        user_name: name,
                    },
                    success: function (response) {
                        // We get the element having id of display_info and put the response inside it
                        $('#display_info1').val(response);
                    }
                });
            }
            else {
                $('#display_info').html("Please Enter Some Words");
            }
        }

    </script>
    </html>
    <?php
    if (isset($_POST['bts']) == 'send') {
        $uid = $_POST['display_info'];

        $id = $_GET['return'];
        $date = date("Y-m-d");
        $quantity = "1";
        $tType = "Return Book";
        $status="returned";
        $returnDate=date("Y-m-d");

        $update="UPDATE `transaction` SET `status`='$status', `returnDate`='$returnDate' WHERE (`bookId`='$id' && `uId`='$uid')";
        $sql = "INSERT INTO `transaction` (`bookId`,`uId`,`tDate`,`quantity`,`tType`) VALUES ('$id','$uid','$date','$quantity','$tType')";
        if (($conn->query($update) === TRUE)&&($conn->query($sql) === TRUE)) {
           ?>
            <script>
                window.location ='http://localhost/lmsphp/bookList.php';
            </script>
        <?php
            echo "successful";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}

$conn->close();
?>
